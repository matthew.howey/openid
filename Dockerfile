FROM node:10-alpine

COPY . src/

WORKDIR src/

RUN npm install

EXPOSE 8000

CMD [ "node", "app.js" ]
