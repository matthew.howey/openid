const express = require("express");
 
const router = express.Router();
 
// Log a user out
router.get('/', (req, res) => {
  res.render('dashboard.njk');
});
 
module.exports = router;