#!/bin/bash

# any future command that fails will exit the script
set -e

# Lets write the public key of our aws instance
# eval $(ssh-agent -s)
# echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
# cwd=$(pwd)
# cd /root
# mkdir .ssh

echo -e "$PRIVATE_KEY" > ./aws-learning-mh.pem
echo -e "$GITLAB_PRIVATE_KEY" > ./id_rsa_2
echo "*******THIS IS ls AFTER CREATING ID RSA 2******"
ls
ALL_SERVERS=(${DEPLOY_SERVERS_1//,/ })

chmod 400 ./aws-learning-mh.pem
# chmod 400 ./id_rsa_2
# chmod 600 /root/.ssh/id_rsa
# cd $cwd
# ** End of alternative approach

# disable the host key checking.
chmod a=rx ./deploy/disableHostKeyChecking.sh
./deploy/disableHostKeyChecking.sh

# we have already setup the DEPLOYER_SERVER in our gitlab settings which is a
# comma seperated values of ip addresses.
DEPLOY_SERVERS=$DEPLOY_SERVERS

echo -e "SESSION_SECRET=${SESSION_SECRET}" >> ./.env
echo -e "OKTA_URL=${OKTA_URL}" >> ./.env
echo -e "OKTA_TOKEN=${OKTA_TOKEN}" >> ./.env
echo -e "OIDC_CLIENT_ID=${OIDC_CLIENT_ID}" >> ./.env
echo -e "OIDC_CLIENT_SECRET=${OIDC_CLIENT_SECRET}" >> ./.env
echo -e "BASE_URL=http://${ALL_SERVERS}" >> ./.env

# lets split this string and convert this into array
# In UNIX, we can use this commond to do this
# ${string//substring/replacement}
# our substring is "," and we replace it with nothing.
echo "ALL_SERVERS ${ALL_SERVERS}"

# Lets iterate over this array and ssh into each EC2 instance
# Once inside the server, run updateAndRestart.sh
for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  cat ./.env
  scp -i ./aws-learning-mh.pem ./id_rsa_2 ec2-user@${server}:.
  scp -i ./aws-learning-mh.pem ./.env ec2-user@${server}:.
  ssh -i ./aws-learning-mh.pem ec2-user@${server} 'bash' < ./deploy/updateAndRestart.sh
done

