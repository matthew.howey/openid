#!/bin/bash

# any future command that fails will exit the script
set -e
echo *** IN UPDATE AND RESTART SCRIPT ***
sudo rm -rf openid
echo "verify in ec2 for openid" > verify.txt

# clone the repo again
sudo yum install git -y
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install 8.9.0
node -e "console.log('Running Node.js ' + process.version)"

sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8000

echo "$DEPLOY_SERVERS_1"

chmod 400 ./id_rsa_2

GIT_SSH_COMMAND="ssh -i ./id_rsa_2" git clone git@gitlab.com:matthew.howey/openid.git

chmod 777 ./id_rsa_2
rm ./id_rsa_2

# stop the previous pm2
npm install pm2 -g
pm2 kill
npm remove pm2 -g

#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
npm install pm2 -g
# starting pm2 daemon
pm2 status

cp ./.env openid/

cd openid

#install npm packages
echo "Running npm install"
npm install

pm2 start ./app.js
