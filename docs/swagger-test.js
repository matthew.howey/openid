const express = require('express');

const swaggerUi = require('swagger-ui-express');
const yamljs = require('yamljs');

const swagger = yamljs.load(`${__dirname}/swagger.yaml`);

const { serve } = swaggerUi;
const setup = swaggerUi.setup(swagger);

const app = express();

app.use('/docs', serve, setup);

app.listen(8001, () => {
  console.log('listening on port 8001');
});