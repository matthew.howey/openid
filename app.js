const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const session = require('express-session');
const okta = require("@okta/okta-sdk-nodejs");
const ExpressOIDC = require("@okta/oidc-middleware").ExpressOIDC;
const nunjucks = require('nunjucks');
require('dotenv').config();

const app = express();

nunjucks.configure('views', {
    autoescape: true,
    express: app
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:8001"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
  secret: process.env.SESSION_SECRET,
  resave: true,
  saveUninitialized: false
}));

var oktaClient = new okta.Client({
  orgUrl: process.env.OKTA_URL,
  token: process.env.OKTA_TOKEN,
});
  
const oidc = new ExpressOIDC({
  issuer: `${process.env.OKTA_URL}/oauth2/default`,
  client_id: process.env.OIDC_CLIENT_ID,
  client_secret: process.env.OIDC_CLIENT_SECRET,
  redirect_uri: `${process.env.BASE_URL}/users/callback`,
  scope: 'openid profile',
  routes: {
    login: {
      path: "/users/login"
    },
    callback: {
      path: "/users/callback",
      defaultRedirect: "/dashboard"
    }
  }
});

app.use(oidc.router);

app.use((req, res, next) => {
  console.log('userinfo', req.userinfo);
  if (!req.userinfo) {
    return next();
  }
  oktaClient.getUser(req.userinfo.sub).then((user) => {
    req.user = user;
    res.locals.user = user;
    next();
  }).catch((err) => {
    next(err);
  })
});

const loginRequired = (req, res, next) => {
  if (!req.user) {
    return res.status(401).render('unauthenticated.njk');
  }
  next();
}

app.get('/test', (req, res) => {
  res.json({ profile: req.user ? req.user.profile : null });
});

const dashboardRouter = require("./routes/dashboard");
const publicRouter = require("./routes/public");
const usersRouter = require("./routes/users");

app.use('/', publicRouter);
app.use('/dashboard', loginRequired, dashboardRouter);
app.get('/sample-without-login', (req, res) => {
  res.send('hello from sample');
});
app.get('/sample-with-login', loginRequired, (req, res) => {
  res.send('hello from sample with login');
});
app.use('/users', usersRouter);

app.listen(8000, () => {
  console.log('Listening on port 8000');
});